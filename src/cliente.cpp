#include "cliente.hpp"
#include <iostream>

using namespace std;

Cliente::Cliente(){

	set_nome("");
	set_data_de_nascimento("");
	set_RG(0);
	set_CPF(0);
	set_email("");
}

Cliente::~Cliente(){

}

string Cliente::get_nome(){
	return nome;
}
void Cliente::set_nome(string nome){
	this->nome = nome;
}

string Cliente::get_data_de_nascimento(){
	return data_de_nascimento;
}
void Cliente::set_data_de_nascimento(string data_de_nascimento){
	this->data_de_nascimento = data_de_nascimento;
}

long int Cliente::get_RG(){
	return RG;
}
void Cliente::set_RG(long int RG){
	this->RG = RG;
}

long int Cliente::get_CPF(){
	return CPF;
}
void Cliente::set_CPF(long int CPF){
	this->CPF = CPF;
}

string Cliente::get_email(){
	return email;
}
void Cliente::set_email(string email){
	this->email = email;
}

