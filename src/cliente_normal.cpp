#include "cliente_normal.hpp"
#include <iostream>

Cliente_normal::Cliente_normal(){
	set_nome("");
	set_data_de_nascimento("");
	set_RG(0);
	set_CPF(0);
	set_email("");
}


Cliente_normal::Cliente_normal(string nome, string data_de_nascimento, long int rg, long int cpf, string email){
	set_nome(nome);
	set_data_de_nascimento(data_de_nascimento);
	set_RG(rg);
	set_CPF(cpf);
	set_email(email);
}

Cliente_normal::~Cliente_normal(){
	
}

void Cliente_normal::mostrar_cliente(){

	cout << "Nome: " << get_nome() << endl;
	cout << "Data de nascimento: " << get_data_de_nascimento() << endl;
	cout << "RG: " << get_RG() << endl;
	cout << "CPF: " << get_CPF() << endl;
	cout << "Email: " << get_email();
}

void Cliente_normal::falar(){

	string bee = "\U0001F41D";

	cout<<endl<<endl<<endl<<" "+bee + "         " +bee + "       " +bee + "       " +bee + "       " +bee + "      " +bee << endl;
	cout<<bee + " Que triste, ele nao ganha desconto hihihi" + bee<<endl;
	cout <<" "+bee + "         " +bee + "       " +bee + "       " +bee + "       " +bee + "      " +bee << endl;
	
}
