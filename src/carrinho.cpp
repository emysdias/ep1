#include "carrinho.hpp"
#include <iostream>

Carrinho::Carrinho(){

}

Carrinho::~Carrinho(){

}

int Carrinho::verificar_estoque(int quero, int tenho){

	int a;
	if(quero > tenho){
		return -1;
	}else{
		a = tenho-quero;
		return a;
	}
}

float Carrinho::desconto(float valor){

	float calc;

	calc = valor*0.15*0;
	return calc;

}

string Carrinho::ListarProduto(string produto){
	return produto;
}

float Carrinho::listarValores(float valor){
	return valor;
}
int Carrinho::listarQuantidade(int quantidade){

	return quantidade;
}
float Carrinho::listarValorTotaldosProdutos(float valor, int quantidade){

	return valor*quantidade;
}
float Carrinho::valorFinaldaVenda(float valor, float desconto){
	
	float calc=0.0f;
	calc = valor-desconto;

	return calc;

}