#include "produto.hpp"
#include <iostream>
#include <fstream>


using namespace std;

Produto::Produto(){
	set_nome("");
	set_codigo(0);
	set_categoria("");
	set_valor(0.0f);
	set_quantidade(0);
}

Produto::Produto(string nome, long int codigo, string categoria){
	set_nome(nome);
	set_codigo(codigo);
	set_categoria(categoria);
	set_valor(0.0f);
	set_quantidade(0);
}

Produto::Produto(string nome, long int codigo, string categoria, float valor, int quantidade){
	set_nome(nome);
	set_codigo(codigo);
	set_categoria(categoria);
	set_valor(valor);
	set_quantidade(quantidade);
}

Produto::~Produto(){

}

string Produto::get_nome(){
	return nome;
}
void Produto::set_nome(string nome){
	this->nome = nome;
}

long int Produto::get_codigo(){
	return codigo;
}
void Produto::set_codigo(long int codigo){
	this->codigo = codigo;
}

string Produto::get_categoria(){
	return categoria;
}
void Produto::set_categoria(string categoria){
	this->categoria = categoria;
}

float Produto::get_valor(){
	return valor;
}
void Produto::set_valor(float valor){
	this->valor = valor;
}
int Produto::get_quantidade(){
	return quantidade;
}
void Produto::set_quantidade(int quantidade){
	this->quantidade = quantidade;
}

void Produto::printar_estoque(){

	cout << "============================" << endl;
	cout << " Eh sucessooooo" << endl;
	cout << "============================" << endl << endl;
	cout << "nome: " << get_nome() << endl;
	cout << "codigo: " << get_codigo() << endl;
	cout << "categoria: " << get_categoria() << endl;
	cout << "valor: " << get_valor() << endl;
	cout << "quantidade: " << get_quantidade() << endl;

}