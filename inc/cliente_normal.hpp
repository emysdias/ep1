#ifndef CLIENTE_NORMAL_HPP
#define CLIENTE_NORMAL_HPP
#include <string>
#include "cliente.hpp"


using namespace std;

class Cliente_normal : public Cliente{


	public:
		Cliente_normal();
		Cliente_normal(string nome, string data_de_nascimento, long int rg, long int cpf, string email);
		~Cliente_normal();
		void mostrar_cliente();
		void falar();
};

#endif