#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>

using namespace std;

class Produto{

	private:
		string nome;
		long int codigo;
		string categoria;
		float valor;
		int quantidade;


	public:
		Produto();
		Produto(string nome, long int codigo, string categoria);
		Produto(string nome, long int codigo, string categoria, float valor, int quantidade);
		~Produto();
		string get_nome();
		void set_nome(string nome);
		long int get_codigo();
		void set_codigo(long int codigo);
		string get_categoria();
		void set_categoria(string categoria);
		float get_valor();
		void set_valor(float valor);
		int get_quantidade();
		void set_quantidade(int quantidade);

		void printar_estoque();


};

#endif