#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <string>

using namespace std;

class Cliente{


	private:
		string nome;
		string data_de_nascimento;
		long int RG;
		long int CPF;
		string email;


	public:
		Cliente();
		~Cliente();
		string get_nome();
		void set_nome(string nome);
		string get_data_de_nascimento();
		void set_data_de_nascimento(string data_de_nascimento);
		long int get_RG();
		void set_RG(long int RG);
		long int get_CPF();
		void set_CPF(long int CPF);
		string get_email();
		void set_email(string email);


		virtual void mostrar_cliente() = 0;
		virtual void falar() = 0;

};

#endif
