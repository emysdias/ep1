#ifndef SOCIO_HPP
#define SOCIO_HPP

#include <string>
#include "cliente.hpp"

using namespace std;


class Socio : public Cliente{

	private:
		long int codigo;

	public:
		Socio();
		Socio(string nome,long int codigo, string data_de_nascimento, long int rg, long int cpf, string email);
		~Socio();
		long int get_codigo();
		void set_codigo(long int codigo);
		void mostrar_cliente();
		float desconto(float valor);
		void falar();


};
#endif