#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <string>

using namespace std;

class Carrinho{

	public:
		Carrinho();
		~Carrinho();


		int verificar_estoque(int quero, int tenho);
		float desconto(float valor);
		string ListarProduto(string produto);
		int listarQuantidade(int quantidade);
		float listarValores(float valor);
		float listarValorTotaldosProdutos(float valor, int quantidade);
		float valorFinaldaVenda(float valor, float desconto);

};
#endif