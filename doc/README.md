# EP1 - OO 2019.2 (UnB - Gama)

Turmas Renato e Carla
Data de entrega: 01/10/2019

## Descrição

Victoria possui um pequeno comércio que atende a população de seu bairro. Com o passar do tempo, a pequena empreendedora foi adquirindo experiência e, por conta de seu excelente poder de negociação, ela conseguia reduzir significativamente o preço dos produtos oferecidos.

Entretanto, apenas preços baixos não eram o bastante para manter a clientela. Em uma noite de inspiração, Victoria pensou em duas estratégias para atrair mais pessoas:
- Oferecer descontos de 15% para clientes sócios;
- Oferecer produtos recomendados exclusivamente para cada cliente;

Para colocar as ideias em prática, ela deve abandonar seu velho hábito de utilizar seu estimado caderninho para gerenciar sua loja. Uma amiga te recomendou para desenvolver um *software* que ajude o estabelecimento a implementar as novas estratégias de negócio. Com a sua ajuda, **todas as vendas serão realizadas pelo computador operado por uma funcionária**. Em uma breve conversa com Victoria, foi possível entender algumas características importantes do sistema:
- Victoria está aprendendo C++ em um curso online e, portanto, prefere que o sistema seja feito nesta linguagem para que ela consiga fazer as próprias manutenções quando necessário;
- Devem existir três modos de operação do sistema:
    - **Modo venda**
    - **Modo recomendação**
    - **Modo estoque**

### Modo venda
Em relação ao modo venda, toda vez que for comprar algo opção (1)Compras, deve ser digitado o email que foi cadastrado na opção dois primeiro (2) Cadastrar cliente, e toda vez que comprar, no arquivo será diminuído a quantidade do produto, não será mostrado na tela, mas é isto que acontece, se quiser verificar, na pasta doc está todos os arquivos, abra o nome da categoria em qual o produto se encontra, e lá será diminuído toda vez q for comprado. E cada vez que quiser acrescentar no carrinho produtos para o cliente, deverá passar pelo mesmo processo, de clicar em compras, digitar o email, e nessa opção cada vez q comprar vai mostrar o valor que foi comprado na hora e o valor total dos produtos. Se em alguma vez digitar a quantidade de produto insuficiente, a compra será cancelada.
Na opção (2) Cadastrar cliente, não pode digitar email que já foi cadastrado antes, se não ele dará o aviso e acabará com essa opção e irá para o menu.

### Modo estoque
No modo estoque será aberto um arquivo para cada categoria do estoque, não pode digitar o mesmo código e produtos diferentes e o inverso também, quando tiver a mesma categoria. Cada vez que cadastrar o produto, será aberto um arquivo com o nome da categoria do produto, e os atributos do produto estarão dentro dele. 
Para cadastrar o mesmo produto com categoria diferente, basta digitar o mesmo código e o nome do produto para ter categorias diferentes, mas produtos iguais.

### Modo recomendação
O modo recomendação mostrará a quantidade de vezes que o produto foi comprado pelo cliente e o nome do produto, basta digitar o email correto do cliente.


=========================================================================================================================

Meu programa é capaz de fazer um gerenciamento de uma loja, com estoques, vendas e recomendações.
Ele é rodado fazendo o git clone do repositório onde se encontra, no gitlab, com o git clone, o programa pode ser rodado com o comando make e logo após o make run.

Quando abre o projeto, ele contém pastas e na pasta chamada src contém os arquivos .cpp, já na pasta inc contém os arquivos .hpp e por fim na pasta doc que acabará sendo salvo os arquivos txts.

No início do programa será inicializado alguns arquivos e com eles que vai ser trabalhado o ep1, e arquivo sócio terá todas as características do cliente sócio que será perguntado durante o programa, e com o cliente_normal igualmente.
Será aberto arquivos no decorrer da execução do programa de acordo com o que for digitado, será aberto txts com nomes de emails e de categorias.



bibliotecas: <iostream>, <stdio.h>, <limits>, <string>, <string.h>, <vector>, <fstream>,
<stdlib.h>, <cstring>, <bits/stdc++.h>;

menu:
    - **1 Modo venda**    Será o controle das vendas
    - **2 Modo recomendação**    Será a recomendação de produtos para o cliente.
    - **3 Modo estoque**    Será para adicionar os estoques, ou retocar o estoque.
    - **0 Sair**    Será para sair do programa.